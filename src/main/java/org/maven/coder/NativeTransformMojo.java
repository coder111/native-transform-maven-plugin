package org.maven.coder;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;

/**
 * Run a native command in order to transform a source file.
 * <p>
 * For example:
 * <pre>
 * &lt;plugin&gt;
 *     &lt;groupId&gt;org.maven.coder&lt;/groupId&gt;
 *     &lt;artifactId&gt;native-transform-maven-plugin&lt;/artifactId&gt;
 *     &lt;executions&gt;
 *         &lt;execution&gt;
 *             &lt;phase&gt;compile&lt;/phase&gt;
 *             &lt;goals&gt;
 *                 &lt;goal&gt;run&lt;/goal&gt;
 *             &lt;/goals&gt;
 *             &lt;configuration&gt;
 *                 &lt;command&gt;sort ${input} -o ${output}&lt;/command&gt;
 *                 &lt;outputExtension&gt;txt&lt;/outputExtension&gt;
 *                 &lt;inputFiles&gt;
 *                     &lt;directory&gt;${project.basedir}/src/main/resources&lt;/directory&gt;
 *                     &lt;includes&gt;
 *                         &lt;include&gt;**&#47;*.raw&lt;/include&gt;
 *                     &lt;/includes&gt;
 *                 &lt;/inputFiles&gt;
 *             &lt;/configuration&gt;
 *         &lt;/execution&gt;
 *     &lt;/executions&gt;
 * &lt;/plugin&gt;
 * </pre>
 */
@Mojo(name = "run", defaultPhase = LifecyclePhase.COMPILE)
public class NativeTransformMojo implements org.apache.maven.plugin.Mojo {

    private Log log;

    @Inject
    private FileSetManager fileSetManager;

    /**
     * Command to execute. ${input} and ${output} will be replaced
     * by input file name and output file name respectively.
     */
    @Parameter(required = true, property = "native.command")
    private String command;

    /**
     * Extension for the output files. Input files will have their
     * extension replaced by this to get output file names
     */
    @Parameter(required = true, property = "native.out.extension")
    private String outputExtension;

    /**
     * Output directory for transformed files. By default it's
     * target/classes
     */
    @Parameter(
            defaultValue = "${project.build.outputDirectory}",
            required = true)
    private File outputDirectory;

    /**
     * Redirect stderr to stdout from executed command. Logs
     * less errors.
     */
    @Parameter(defaultValue = "false")
    private boolean errorToOutput = false;

    /**
     * Input files to transform.
     */
    @Parameter(required = true)
    private FileSet inputFiles;

    @Override
    public void execute() {
        String[] files = fileSetManager.getIncludedFiles(inputFiles);
        if (files == null) {
            return;
        }
        for (String f : files) {
            Path input = Paths.get(inputFiles.getDirectory(), f);

            Path output = Paths.get(outputDirectory.getAbsolutePath(), f);
            output = CommandExecutor.replaceExtension(output, outputExtension);

            try {
                if (Files.exists(output)) {
                    FileTime inputTime = Files.getLastModifiedTime(input);
                    FileTime outputTime = Files.getLastModifiedTime(output);
                    if (outputTime.compareTo(inputTime) >= 0) {
                        // regenerate 0 length files
                        if (output.toFile().length() > 0) {
                            log.info("Output already exists and is fresh " + output);
                            continue;
                        }
                    }
                }

                CommandExecutor cmd = new CommandExecutor(this.log, command, input, output, this.errorToOutput);
                boolean success = cmd.run();
                if (!success) {
                    throw new RuntimeException("Command execution failed");
                }
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    @Override
    public void setLog(Log log) {
        this.log = log;
    }

    @Override
    public Log getLog() {
        return log;
    }
}
