# native-transform-maven-plugin

Maven plugin to run a native process to transform/compile a file. For example:

```xml
<plugin>
    <groupId>org.maven.coder</groupId>
    <artifactId>native-transform-maven-plugin</artifactId>
    <executions>
        <execution>
            <phase>compile</phase>
            <goals>
                <goal>run</goal>
            </goals>
            <configuration>
                <command>sort ${input} -o ${output}</command>
                <outputExtension>txt</outputExtension>
                <inputFiles>
                    <directory>${project.basedir}/src/main/resources</directory>
                    <includes>
                        <include>**/*.raw</include>
                    </includes>
                </inputFiles>
            </configuration>
        </execution>
    </executions>
</plugin>
```

TODO: more tests

TODO: Publish to Maven Centra. HOWTO:

https://maven.apache.org/repository/guide-central-repository-upload.html

https://central.sonatype.org/pages/requirements.html#sufficient-metadata
